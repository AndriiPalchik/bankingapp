﻿using System.Data;
using System.Data.Entity;
using BankingApp.Repository.DataContext;
using BankingApp.Repository.Repository;

namespace BankingApp.Repository.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly DbDataContext _db;
        private readonly DbContextTransaction _transaction;

        public UnitOfWork(IsolationLevel? isolationLevel = null)
        {
            _db = new DbDataContext();

            if (isolationLevel != null)
                _transaction = _db.Database.BeginTransaction(isolationLevel.Value);
            else
                _transaction = _db.Database.BeginTransaction();
        }

        private void Commit()
        {
            _transaction.Commit();
        }

        public void RollbackChanges()
        {
            _transaction.Rollback();
        }

        public IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if(_userRepository == null)
                    _userRepository = new UserRepository(_db);

                return _userRepository;
            }
        }

        public ITransactionRepository _transactionRepository;
        public ITransactionRepository TransactionRepository
        {
            get
            {
                if (_transactionRepository == null)
                    _transactionRepository = new TransactionRepository(_db);

                return _transactionRepository;
            }
        }

        public void Dispose()
        {
            _db.SaveChanges();
            Commit();
            _transaction.Dispose();
        }
    }
}