﻿namespace BankingApp.Repository.Uow
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork CreateNewUnitOfWork()
        {
            return new UnitOfWork();
        }
    }
}