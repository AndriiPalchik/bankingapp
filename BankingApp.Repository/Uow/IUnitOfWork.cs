﻿using System;
using BankingApp.Repository.DataContext;
using BankingApp.Repository.Repository;

namespace BankingApp.Repository.Uow
{
    public interface IUnitOfWork : IDisposable
    {
        void RollbackChanges();
        IUserRepository UserRepository { get; }
        ITransactionRepository TransactionRepository { get; }
    }
}