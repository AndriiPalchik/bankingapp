﻿using System.Collections.Generic;
using System.Linq;
using BankingApp.Entities;
using BankingApp.Repository.DataContext;

namespace BankingApp.Repository.Repository
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(DbDataContext dataContext)
            : base(dataContext)
        {
        }

        public User GetUser(int userId)
        {
            return _dataContext.User.FirstOrDefault(x => x.Id == userId);
        }

        public User GetUser(string userName, string password)
        {
            return _dataContext.User.FirstOrDefault(x => x.Name == userName && x.Password == password);
        }

        public List<User> GetUsers(int userIdToExclude)
        {
            return _dataContext.User.Where(x => x.Id != userIdToExclude).OrderBy(x => x.Name).Select(x => x).ToList();
        }

        public bool UserExists(string userName)
        {
            return _dataContext.User.Any(x => x.Name == userName);
        }

        public void CreateUser(User user)
        {
            _dataContext.User.Add(user);
        }
    }
}