﻿using BankingApp.Repository.DataContext;
using BankingApp.Repository.Uow;

namespace BankingApp.Repository.Repository
{
    public class RepositoryBase
    {
        protected readonly DbDataContext _dataContext;

        public RepositoryBase(DbDataContext dataContext)
        {
            _dataContext = dataContext;
        }
    }
}