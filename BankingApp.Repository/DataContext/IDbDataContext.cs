using System.Data.Entity;
using BankingApp.Entities;

namespace BankingApp.Repository.DataContext
{
    public interface IDbDataContext
    {
        DbSet<User> User { get; set; }
        DbSet<Transaction> Transaction { get; set; }
    }
}