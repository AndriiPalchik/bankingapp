﻿using System.Data.Entity;
using BankingApp.Common.Configuration;
using BankingApp.Entities;

namespace BankingApp.Repository.DataContext
{
    public class DbDataContext : DbContext, IDbDataContext
    {
        public DbDataContext() : base(AppSettings.GetInstance().ConnectionString)
        {
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Transaction> Transaction { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(x => x.Transactions)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.UserId);
        }
    }
}