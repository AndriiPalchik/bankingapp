﻿using System.Collections.Generic;
using BankingApp.Common.Models;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services
{
    public interface IBankAcountDataService
    {
        RequestResult<double> GetUserBalance(int userId);
        RequestResult<double> Deposite(int userId, double amount);
        RequestResult<double> Withdraw(int userId, double amount);
        RequestResult<List<TransactionModel>> GetUserTransactions(int userId);
        RequestResult<double> Transfer(int fromUserId, TransferModel transferModel);
    }
}