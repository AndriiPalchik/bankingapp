using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using BankingApp.Common.Definitions;
using BankingApp.Common.Models;
using BankingApp.Entities;
using BankingApp.Repository.Uow;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services.Impl
{
    public class BankAcountDataService : IBankAcountDataService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BankAcountDataService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public RequestResult<double> GetUserBalance(int userId)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var user = uow.UserRepository.GetUser(userId);

                    if (user != null)
                    {
                        return new RequestResult<double>(RequestResultStatus.Success, user.AccountBalance);
                    }
                    else
                    {
                        return new RequestResult<double>(RequestResultStatus.Error, 0, "User is not found");
                    }
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return new RequestResult<double>(RequestResultStatus.Error, 0, "User balance is not available");
        }

        public RequestResult<double> Deposite(int userId, double amount)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var user = uow.UserRepository.GetUser(userId);

                    if (user != null)
                    {
                        user.AccountBalance += amount;
                        AddTransaction(user, amount);
                    }
                    else
                    {
                        return new RequestResult<double>(RequestResultStatus.Error, 0, "User is not found");
                    }
                }

                return GetUserBalance(userId);
            }
            catch (DbUpdateConcurrencyException)
            {
                Deposite(userId, amount);
            }
            catch (Exception)
            {
                //logg error
            }

            return new RequestResult<double>(RequestResultStatus.Error, 0, "Cannot deposite");
        }

        public RequestResult<double> Withdraw(int userId, double amount)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var user = uow.UserRepository.GetUser(userId);

                    if (user != null)
                    {
                        if (user.AccountBalance >= amount)
                        {
                            user.AccountBalance -= amount;
                            AddTransaction(user, -amount);
                        }
                        else
                        {
                            return new RequestResult<double>(RequestResultStatus.Error, 0, "Not enough money");
                        }
                    }
                    else
                    {
                        return new RequestResult<double>(RequestResultStatus.Error, 0, "User is not found");
                    }
                }

                return GetUserBalance(userId);
            }
            catch (DbUpdateConcurrencyException)
            {
                Withdraw(userId, amount);
            }
            catch (Exception)
            {
                //logg error
            }

            return new RequestResult<double>(RequestResultStatus.Error, 0, "Cannot Withdraw");
        }

        public RequestResult<double> Transfer(int fromUserId, TransferModel transferModel)
        {
            if (transferModel == null || transferModel.ToUserId <= 0 || transferModel.Amount <= 0)
            {
                return new RequestResult<double>(RequestResultStatus.Error, 0, "Model is not valid");
            }

            if (fromUserId == transferModel.ToUserId)
                return new RequestResult<double>(RequestResultStatus.Error, 0, "Cannot transfer to your account");

            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var fromUser = uow.UserRepository.GetUser(fromUserId);
                    var toUser = uow.UserRepository.GetUser(transferModel.ToUserId);

                    if (fromUser != null && toUser != null)
                    {
                        if (fromUser.AccountBalance >= transferModel.Amount)
                        {
                            fromUser.AccountBalance -= transferModel.Amount;
                            toUser.AccountBalance += transferModel.Amount;

                            AddTransaction(fromUser, -transferModel.Amount);
                            AddTransaction(toUser, transferModel.Amount);
                        }
                        else
                        {
                            return new RequestResult<double>(RequestResultStatus.Error, 0, "Not enough money");
                        }
                    }
                    else
                    {
                        return new RequestResult<double>(RequestResultStatus.Error, 0, "User is not found");
                    }
                }

                return GetUserBalance(fromUserId);
            }
            catch (DbUpdateConcurrencyException)
            {
                Transfer(fromUserId, transferModel);
            }
            catch (Exception)
            {
                //logg error
            }

            return new RequestResult<double>(RequestResultStatus.Error, 0, "Cannot Transfer");
        }

        public RequestResult<List<TransactionModel>> GetUserTransactions(int userId)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var transactionModels = uow.TransactionRepository.GetUserTransactions(userId)
                        .Select(x => new TransactionModel() {Amount = x.Amount, CreateDate = x.CreateDate}).ToList();

                    return new RequestResult<List<TransactionModel>>(RequestResultStatus.Success, transactionModels);
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return new RequestResult<List<TransactionModel>>(RequestResultStatus.Error, null, "User balance is not available");
        }

        public void AddTransaction(User user, double amount)
        {
            if(user.Transactions == null)
                user.Transactions = new Collection<Transaction>();

            user.Transactions.Add(new Transaction() { Amount = amount });
        }
    }
}