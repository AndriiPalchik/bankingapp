﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankingApp.Common.Definitions;
using BankingApp.Common.Models;
using BankingApp.Entities;
using BankingApp.Repository.Uow;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services.Impl
{
    public class UserDataService : IUserDataService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UserDataService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public bool UserExists(string userName)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    return uow.UserRepository.UserExists(userName);
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return false;
        }

        public bool CreateUser(User user)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    uow.UserRepository.CreateUser(user);
                }

                return true;
            }
            catch (Exception)
            {
                //logg error
            }

            return false;
        }

        public User GetUser(string userName, string password)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    return uow.UserRepository.GetUser(userName, password);
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return null;
        }

        public User GetUser(int userId)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    return uow.UserRepository.GetUser(userId);
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return null;
        }

        public RequestResult<List<UserModel>> GetUsers(int userIdToExclude)
        {
            try
            {
                using (var uow = _unitOfWorkFactory.CreateNewUnitOfWork())
                {
                    var userModels = uow.UserRepository.GetUsers(userIdToExclude)
                        .Select(x => new UserModel() { Id = x.Id, Name = x.Name }).ToList();

                    return new RequestResult<List<UserModel>>(RequestResultStatus.Success, userModels);
                }
            }
            catch (Exception)
            {
                //logg error
            }

            return null;
        }
    }
}