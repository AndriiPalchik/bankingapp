﻿using System;
using BankingApp.Common.Definitions;
using BankingApp.Common.HashProviders;
using BankingApp.Common.Models;
using BankingApp.Entities;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services.Impl
{
    public class AccountDataService : IAccountDataService
    {
        private readonly IHashProvider _hashProvider;
        private readonly IUserDataService _userDataService;

        public AccountDataService(IHashProvider hashProvider, IUserDataService userDataService)
        {
            _hashProvider = hashProvider;
            _userDataService = userDataService;
        }

        public RequestResult<UserModel> RegisterUser(UserRegisterModel userRegisterModel)
        {
            var requestresult = new RequestResult<UserModel>();

            if (userRegisterModel == null || string.IsNullOrWhiteSpace(userRegisterModel.Name) || string.IsNullOrWhiteSpace(userRegisterModel.Pwd))
            {
                requestresult.Set(RequestResultStatus.Error, null, "Model is not valid");
                return requestresult;
            }

            try
            {
                if (_userDataService.UserExists(userRegisterModel.Name))
                {
                    requestresult.Set(RequestResultStatus.Duplicate, null, "User exists");
                    return requestresult;
                }

                var user = new User() { Name = userRegisterModel.Name, Password = _hashProvider.GetHash(userRegisterModel.Pwd) };

                if (_userDataService.CreateUser(user))
                {
                    var userModel = new UserModel()
                    {
                        Name = user.Name,
                        Id = user.Id
                    };
                    requestresult.Set(RequestResultStatus.Success, userModel);
                    return requestresult;
                }
            }
            catch (Exception)
            {
                //the error should be logged
            }

            requestresult.Set(RequestResultStatus.Error, null, "Cannot register new user. Please contact system administrator");
            return requestresult;
        }

        public RequestResult<UserModel> Login(UserLoginModel userLoginModel)
        {
            var requestresult = new RequestResult<UserModel>();

            if (userLoginModel == null || string.IsNullOrWhiteSpace(userLoginModel.Name) || string.IsNullOrWhiteSpace(userLoginModel.Pwd))
            {
                requestresult.Set(RequestResultStatus.Error, null, "Model is not valid");
                return requestresult;
            }

            var user = _userDataService.GetUser(userLoginModel.Name, _hashProvider.GetHash(userLoginModel.Pwd));
            if (user != null)
            {
                var userModel = new UserModel()
                {
                    Name = user.Name,
                    Id = user.Id
                };

                requestresult.Set(RequestResultStatus.Success, userModel);
                return requestresult;
            }

            requestresult.Set(RequestResultStatus.Error, null, "Login failed");
            return requestresult;
        }
    }
}