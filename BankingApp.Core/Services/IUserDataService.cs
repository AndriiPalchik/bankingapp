﻿using System.Collections.Generic;
using BankingApp.Common.Models;
using BankingApp.Entities;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services
{
    public interface IUserDataService
    {
        bool UserExists(string userName);
        bool CreateUser(User user);
        User GetUser(string userName, string password);
        User GetUser(int userId);
        RequestResult<List<UserModel>> GetUsers(int userIdToExclude);
    }
}