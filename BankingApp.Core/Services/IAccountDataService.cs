﻿using BankingApp.Common.Models;
using BankingApp.Entities;
using BankingApp.ViewModels;

namespace BankingApp.Core.Services
{
    public interface IAccountDataService
    {
        RequestResult<UserModel> RegisterUser(UserRegisterModel userRegisterModel);
        RequestResult<UserModel> Login(UserLoginModel user);
    }
}