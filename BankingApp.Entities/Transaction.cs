﻿using System;

namespace BankingApp.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public double Amount { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual User User { get; set; }

        public Transaction()
        {
            CreateDate = DateTime.Now;
        }
    }
}