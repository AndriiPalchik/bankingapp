﻿using BankingApp.Common.Definitions;
using BankingApp.Common.HashProviders;
using BankingApp.Core.Services;
using BankingApp.Core.Services.Impl;
using BankingApp.Entities;
using BankingApp.ViewModels;
using Moq;
using NUnit.Framework;

namespace BankingApp.Core.Test.Services.Impl
{
    [TestFixture]
    public class AccountDataServiceTest
    {
        [Test]
        public void Register_ModelIsNotValid_NotRegistered_Test()
        {
            //Arrange
            var userDataService = new Mock<IUserDataService>();
            var service = new AccountDataService(new MD5Hash(), userDataService.Object);
            
            //Act
            var result = service.RegisterUser(null);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.IsNull(result.Obj);
        }

        [Test]
        public void Register_Duplicate_NotRegistered_Test()
        {
            //Arrange
            var userDataService = new Mock<IUserDataService>();
            userDataService.Setup(x => x.UserExists(It.IsAny<string>())).Returns(true);

            var service = new AccountDataService(new MD5Hash(), userDataService.Object);
            
            //Act
            var result = service.RegisterUser(new UserRegisterModel(){Name = "1",Pwd = "1"});

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Duplicate);
            Assert.IsNull(result.Obj);
        }

        [Test]
        public void Register_UserRegistered_Test()
        {
            //Arrange
            var userDataService = new Mock<IUserDataService>();
            userDataService.Setup(x => x.UserExists(It.IsAny<string>())).Returns(false);
            userDataService.Setup(x => x.CreateUser(It.IsAny<User>())).Returns(true);

            var service = new AccountDataService(new MD5Hash(), userDataService.Object);
            var model = new UserRegisterModel() {Name = "1", Pwd = "1"};
            //Act
            var result = service.RegisterUser(model);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Success);
            Assert.IsNotNull(result.Obj);
            Assert.AreEqual(result.Obj.Name, model.Name);
        }

        [Test]
        public void Login_ModelIsNotValid_NotLoggedIn_Test()
        {
            //Arrange
            var userDataService = new Mock<IUserDataService>();

            var service = new AccountDataService(new MD5Hash(), userDataService.Object);
            UserLoginModel model = null;
            //Act
            var result = service.Login(model);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.IsNull(result.Obj);
        }

        [Test]
        public void Login_LoggedIn_Test()
        {
            //Arrange
            var userDataService = new Mock<IUserDataService>();
            userDataService.Setup(x => x.GetUser(It.IsAny<string>(), It.IsAny<string>())).Returns(new User() { Name = "1", Id = 1 });

            var service = new AccountDataService(new MD5Hash(), userDataService.Object);
            var model = new UserLoginModel() {Name = "1", Pwd = "1"};
            //Act
            var result = service.Login(model);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Success);
            Assert.IsNotNull(result.Obj);
            Assert.AreEqual(result.Obj.Name, model.Name);
        }
         
    }
}