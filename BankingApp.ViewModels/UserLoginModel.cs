﻿namespace BankingApp.ViewModels
{
    public class UserLoginModel
    {
        public string Name { get; set; }
        public string Pwd { get; set; }
    }
}