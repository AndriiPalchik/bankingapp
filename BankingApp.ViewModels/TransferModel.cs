﻿namespace BankingApp.ViewModels
{
    public class TransferModel
    {
        public int ToUserId { get; set; }
        public double Amount { get; set; }
    }
}