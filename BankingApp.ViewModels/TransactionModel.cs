﻿using System;

namespace BankingApp.ViewModels
{
    public class TransactionModel
    {
        public double Amount { get; set; }
        public DateTime CreateDate { get; set; }
    }
}