bankingApp.factory('API', [
    '$http',
    'urlsAPI',
    function(
        $http,
        urlsAPI) {

        var API = {};

        API.login = function(login, password) {
            return $http({
                url: urlsAPI.login,
                method: "POST",
                data: {
                    Name: login,
                    Pwd: password
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        };

        API.signup = function(login, password) {
            return $http({
                url: urlsAPI.signup,
                method: "POST",
                data: {
                    Name: login,
                    Pwd: password
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        };

        API.signout = function() {
            return $http({
                url: urlsAPI.signout,
                method: "POST",
                withCredentials: true
            })
        };

        API.balance = function() {
            return $http({
                url: urlsAPI.balance,
                method: "GET",
                withCredentials: true
            })
        };

        API.deposit = function(amount) {
            return $http({
                url: urlsAPI.deposit,
                method: "POST",
                data: {
                    Amount: amount
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        };

        API.withdraw = function(amount) {
            return $http({
                url: urlsAPI.withdraw,
                method: "POST",
                data: {
                    Amount: amount
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        };

        API.users = function() {
            return $http({
                url: urlsAPI.user,
                method: "GET"
            })
        };

        API.transaction = function() {
            return $http({
                url: urlsAPI.transaction,
                method: "GET",
                withCredentials: true
            })
        };

        API.transfer = function(userId, amount) {
            return $http({
                url: urlsAPI.transfer,
                method: "POST",
                data: {
                    ToUserId: userId,
                    Amount: amount
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        };

        return API;
    }
]);
