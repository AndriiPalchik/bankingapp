bankingApp.controller('mainController', function ($rootScope, $scope, $cookies, $location, API) {
    $scope.logined = false;

    function checkAuth() {
        if ($cookies.get('.ASPXAUTH')) {
            $scope.logined = true;
            if ($location.path() === '/login' || $location.path() === '/signup') {
                $location.path('/');
            }
        } else {
            if (!($location.path() === '/login' || $location.path() === '/signup')) {
                $location.path('/login');
            }
            $scope.logined = false;
        }
    };

    $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
        if ($cookies.get('.ASPXAUTH')) {
            $scope.logined = true;
        } else {
            if (!($location.path() === '/login' || $location.path() === '/signup')) {
                $location.path('/login');
            }
            $scope.logined = false;
        }
    });

    /*window.onhashchange = function () {
        checkAuth();
    }

    checkAuth();*/

    $scope.signOut = function () {
        API.signout().success(function () {
            $scope.logined = false;
        });
    };
});

bankingApp.controller('homeController', function ($scope, $location, API) {

    API.balance()
        .success(function (data, status) {
            $scope.balanceLoaded = true;
            $scope.balance = (data.Obj) ? data.Obj : '0';
        })
        .error(function (data, status) {
            $scope.balanceLoaded = true;
            $scope.error = data.Message;
        });
});

bankingApp.controller('loginController', function ($scope, API, $location) {

    $scope.loginQuery = function () {
        $scope.queryStatus = true;
        API.login($scope.login, $scope.password)
            .success(function (data, status) {
                $location.path('/');
                $scope.queryStatus = false;
            })
            .error(function (data, status) {
                $scope.error = data.Message;
                $scope.queryStatus = false;
            });
    };
});

bankingApp.controller('signupController', function ($scope, API, $location) {
    $scope.signupQuery = function () {
        if ($scope.password === $scope.repeatpassword) {
            $scope.queryStatus = true;
            API.signup($scope.login, $scope.password)
                .success(function (data, status) {
                    $location.path('/');
                    $scope.queryStatus = false;
                })
                .error(function (data, status) {
                    $scope.error = data.Message;
                    $scope.queryStatus = false;
                });

        } else {
            $scope.error = "Passwords do not match";
        }
    };
});

bankingApp.controller('statementController', function ($scope, API) {
    API.transaction()
        .success(function (data, status) {
            $scope.transactions = data.Obj;
            console.log(data);
            $scope.statementLoaded = true;
        })
        .error(function (data, status) {
            $scope.statementLoaded = true;
            $scope.error = data.Message;
        });
});

bankingApp.controller('depositController', function ($scope, API) {
    $scope.depositQuery = function () {
        $scope.queryStatus = true;
        API.deposit($scope.amount)
        .success(function (data, status) {
            $scope.success = true;
            $scope.balance = data.Obj;
            $scope.queryStatus = false;
        })
        .error(function (data, status) {
            $scope.error = data.Message;
            $scope.queryStatus = false;
        });
    };

});

bankingApp.controller('withdrawController', function ($scope, API) {
    $scope.withdrawQuery = function () {
        $scope.queryStatus = true;
        API.withdraw($scope.amount)
        .success(function (data, status) {
            $scope.success = true;
            $scope.balance = data.Obj;
            $scope.queryStatus = false;
        })
        .error(function (data, status) {
            $scope.error = data.Message;
            $scope.queryStatus = false;
        });
    };
});

bankingApp.controller('transferController', function ($scope, API) {
    API.users()
        .success(function (data, status) {
            $scope.users = data.Obj;
            $scope.userLoaded = true;
        })
        .error(function (data, status) {
            $scope.userLoaded = true;
            $scope.error = data.Message;
        });

    $scope.withdrawQuery = function () {
        $scope.queryStatus = true;
        API.transfer($scope.user, $scope.amount)
        .success(function (data, status) {
            $scope.queryStatus = false;
            $scope.success = true;
            $scope.balance = data.Obj;
        })
        .error(function (data, status) {
            $scope.queryStatus = false;
            $scope.error = data.Message;
        });
    };
});
