var bankingApp = angular.module('bankingApp', ['ngRoute', 'ngCookies']);

bankingApp.constant("urlsAPI", {
    "login": "/api/v1/Account/Login",
    "signout": "/api/v1/Account/SignOut",
    "signup": "/api/v1/Account/Register",
    "balance": "/api/v1/Balance",
    "deposit": "/api/v1/Deposit",
    "withdraw": "/api/v1/Withdraw",
    "user": "/api/v1/User",
    "transaction": "/api/v1/Transaction",
    "transfer": "/api/v1/Transfer"
})

bankingApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

// configure our routes
bankingApp.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/assets/views/home.html',
        controller: 'homeController'
    })

    // route for the login page
    .when('/login', {
        templateUrl: '/assets/views/login.html',
        controller: 'loginController'
    })   

    // route for the signup page
    .when('/signup', {
        templateUrl: '/assets/views/signup.html',
        controller: 'signupController'
    })

    // route for the statement page
    .when('/statement', {
        templateUrl: '/assets/views/statement.html',
        controller: 'statementController'
    })

        // route for the deposit page
    .when('/deposit', {
        templateUrl: '/assets/views/deposit.html',
        controller: 'depositController'
    })

        // route for the withdraw page
    .when('/withdraw', {
        templateUrl: '/assets/views/withdraw.html',
        controller: 'withdrawController'
    })

        // route for the transfer page
    .when('/transfer', {
        templateUrl: '/assets/views/transfer.html',
        controller: 'transferController'
    })

    .otherwise({ redirectTo: '/' });
});
