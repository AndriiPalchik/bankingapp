﻿using System.Web.Http;
using System.Web.Mvc;
using BankingApp.Common.HashProviders;
using BankingApp.Common.Interfaces;
using BankingApp.Common.InversionOfControl;
using BankingApp.Core.Services;
using BankingApp.Core.Services.Impl;
using BankingApp.Repository.Uow;
using BankingApp.Web.Infrastructure;
using Microsoft.Practices.Unity;

namespace BankingApp.Web.IoC
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise(HttpConfiguration httpConfig)
        {
            var container = BuildUnityContainer();

            var dependencyResolver = new UnityResolver(container);
            DependencyResolver.SetResolver(dependencyResolver);
            httpConfig.DependencyResolver = dependencyResolver;

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = Ioc.Instance;

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            RegisterServices(container);
        }

        private static void RegisterServices(IUnityContainer container)
        {
            container.RegisterType<IAccountDataService, AccountDataService>();
            container.RegisterType<IUserDataService, UserDataService>();
            container.RegisterType<IUserAccessController, UserAccessController>();
            container.RegisterType<IBankAcountDataService, BankAcountDataService>();
            container.RegisterType<IUnitOfWorkFactory, UnitOfWorkFactory>();
            container.RegisterType<IHashProvider, MD5Hash>();
        }
    }
}