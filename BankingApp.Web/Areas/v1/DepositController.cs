﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;
using BankingApp.ViewModels;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class DepositController : ApiController
    {
        private readonly IUserAccessController _userAccessController;
        private readonly IBankAcountDataService _bankAcountDataService;

        public DepositController(IUserAccessController userAccessController, IBankAcountDataService bankAcountDataService)
        {
            _userAccessController = userAccessController;
            _bankAcountDataService = bankAcountDataService;
        }

        [Route("~/api/v1/Deposit"), HttpPost]
        public HttpResponseMessage Deposit(DepositModel depositModel)
        {
            if (depositModel == null || depositModel.Amount <= 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model is not valid");

            var result = _bankAcountDataService.Deposite(_userAccessController.UserId, depositModel.Amount);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}