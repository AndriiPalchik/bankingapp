﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;
using BankingApp.ViewModels;

namespace BankingApp.Web.Areas.v1
{
    [EnableCors(origins: "http://fiddle.jshell.net", headers: "*", methods: "*")]
    public class AccountController : ApiController
    {
        private readonly IAccountDataService _accountDataService;
        private readonly IUserAccessController _userAccessController;

        public AccountController(IAccountDataService accountDataService, IUserAccessController userAccessController)
        {
            _accountDataService = accountDataService;
            _userAccessController = userAccessController;
        }

        [Route("~/api/v1/Account/Login"), HttpPost]
        public HttpResponseMessage Login(UserLoginModel userLoginModel)
        {
            if (userLoginModel == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model is not valid");

            if (HttpContext.Current.User.Identity.IsAuthenticated)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User is Logged in");

            var result = _accountDataService.Login(userLoginModel);

            if (result.Status == RequestResultStatus.Success && _userAccessController.AuthenticateUser(result.Obj.Id))
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }

        [Route("~/api/v1/Account/SignOut"), HttpPost]
        public HttpResponseMessage SignOut()
        {
            _userAccessController.SignOutUser();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("~/api/v1/Account/Register"), HttpPost]
        public HttpResponseMessage Register(UserRegisterModel userRegisterModel)
        {
            if (userRegisterModel == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model is not valid");

            if (HttpContext.Current.User.Identity.IsAuthenticated)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "User is Logged in");

            var result = _accountDataService.RegisterUser(userRegisterModel);

            if (result.Status == RequestResultStatus.Success && _userAccessController.AuthenticateUser(result.Obj.Id))
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}