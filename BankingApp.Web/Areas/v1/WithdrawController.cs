﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;
using BankingApp.ViewModels;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class WithdrawController : ApiController
    {
        private readonly IUserAccessController _userAccessController;
        private readonly IBankAcountDataService _bankAcountDataService;

        public WithdrawController(IUserAccessController userAccessController, IBankAcountDataService bankAcountDataService)
        {
            _userAccessController = userAccessController;
            _bankAcountDataService = bankAcountDataService;
        }

        [Route("~/api/v1/Withdraw"), HttpPost]
        public HttpResponseMessage Withdraw(WithdrawModel withdrawModel)
        {
            if (withdrawModel == null || withdrawModel.Amount <= 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model is not valid");

            var result = _bankAcountDataService.Withdraw(_userAccessController.UserId, withdrawModel.Amount);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}