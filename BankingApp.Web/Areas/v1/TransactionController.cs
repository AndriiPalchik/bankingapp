using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class TransactionController : ApiController
    {
        private readonly IUserAccessController _userAccessController;
        private readonly IBankAcountDataService _bankAcountDataService;

        public TransactionController(IUserAccessController userAccessController, IBankAcountDataService bankAcountDataService)
        {
            _userAccessController = userAccessController;
            _bankAcountDataService = bankAcountDataService;
        }

        [Route("~/api/v1/Transaction"), HttpGet]
        public HttpResponseMessage GetTransactions()
        {
            var result = _bankAcountDataService.GetUserTransactions(_userAccessController.UserId);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}