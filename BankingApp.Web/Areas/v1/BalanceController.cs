﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class BalanceController : ApiController
    {
        private readonly IUserAccessController _userAccessController;
        private readonly IBankAcountDataService _bankAcountDataService;

        public BalanceController(IUserAccessController userAccessController, IBankAcountDataService bankAcountDataService)
        {
            _userAccessController = userAccessController;
            _bankAcountDataService = bankAcountDataService;
        }

        [Route("~/api/v1/Balance"), HttpGet]
        public HttpResponseMessage GetCurrentBalance()
        {
            var result = _bankAcountDataService.GetUserBalance(_userAccessController.UserId);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}