﻿namespace BankingApp.Common.Interfaces
{
    public interface IUserAccessController
    {
        bool AuthenticateUser(int userId);
        void SignOutUser();
        void PostAuthRequest();
        int UserId { get; }
    }
}