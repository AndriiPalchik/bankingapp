﻿using Microsoft.Practices.Unity;

namespace BankingApp.Common.InversionOfControl
{
    public class Ioc
    {
        public static IUnityContainer _container;
        public static object _createInstanceLocker = new object();

        public static IUnityContainer Instance
        {
            get
            {
                if (_container == null)
                {
                    lock (_createInstanceLocker)
                    {
                        if (_container == null)
                            _container = new UnityContainer();
                    }
                }

                return _container;
            }
        }

        public static T Get<T>()
        {
            return Instance.Resolve<T>();
        }
    }
}