namespace BankingApp.Common.HashProviders
{
    public interface IHashProvider
    {
        string GetHash(string str);
    }
}