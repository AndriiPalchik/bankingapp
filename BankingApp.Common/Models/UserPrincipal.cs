﻿using System;
using System.Security.Principal;
using BankingApp.Common.Interfaces;

namespace BankingApp.Common.Models
{
    [Serializable]
    public class UserPrincipal : IUserPrincipal, IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public UserPrincipal(IUserPrincipal tophandsPrincipal)
        {
            UserId = tophandsPrincipal.UserId;
            Name = tophandsPrincipal.Name;

            Identity = new GenericIdentity(Name);
        }

        public UserPrincipal(int userId, string userName) : this(new UserPrincipalSerializeModel() { UserId = userId, Name = userName }) { }

        public bool IsInRole(string role)
        {
            return false;
        }

        public int UserId { get; set; }
        public string Name { get; set; }
    }
}